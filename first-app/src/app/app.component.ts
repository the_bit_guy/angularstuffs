import { Component } from '@angular/core';

//TypeScript code
//creates the component & binds the variables declared in the tempalteUrl file.
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title1 = 'app works! Yeah i got it';
}
